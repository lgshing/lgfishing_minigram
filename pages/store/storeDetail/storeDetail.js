
import { imageUrl } from '../../../utils/params'
var WxParse = require('../../../wxParse/wxParse.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    code: '',
    userData: '',
    article: '',
    phoneNum : "",
    pageIndex : 1,
    foodsList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.Detail(options.objectId);
    this.FoodsList(options.objectId);
    var that = this;
    setData({
      code: options.objectId
    })
    /**
     * 初始化emoji设置
     */
    WxParse.emojisInit('[]', "/wxParse/emojis/", {
      "00": "00.gif",
      "01": "01.gif",
      "02": "02.gif",
      "03": "03.gif",
      "04": "04.gif",
      "05": "05.gif",
      "06": "06.gif",
      "07": "07.gif",
      "08": "08.gif",
      "09": "09.gif",
      "09": "09.gif",
      "10": "10.gif",
      "11": "11.gif",
      "12": "12.gif",
      "13": "13.gif",
      "14": "14.gif",
      "15": "15.gif",
      "16": "16.gif",
      "17": "17.gif",
      "18": "18.gif",
      "19": "19.gif",
    });

  },

  onPullDownRefresh: function () {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    that.setData({
      pageIndex: 1
    })
    that.FoodsList(that.data.code);
  },

  onReachBottom: function () {
    let that = this;
    console.log('加载更多');
    var index = that.data.pageIndex + 1;
    that.setData({
      pageIndex: index
    })
    that.FoodsList(that.data.code);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  Detail: function (objectId) {
    let that = this;
    wx.request({
      url: 'https://www.heikengdiaoyu.com:9405/getstoreinfo',
      data: {
        "storeid": objectId
      },
      method: `GET`,
      header: {
        'content-type': 'application/json', // 默认值
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res)
        var item = res.data.data;
        console.log(item);
      
        var phoneStr = item.connection_method;
        var list = phoneStr.split(':');
        var phone = "";
    if(list[0]=="phone"){
      phone = list[1];
    }
    console.log(phone);
        that.setData({
          userData: item,
          phoneNum: phone
        })
        WxParse.wxParse('article', 'html', item.html_content, that, 5);
      }
    })
  },

  FoodsList: function (objectId) {
    let that = this;
    wx.request({
      url: 'https://www.heikengdiaoyu.com:9405/getfoodsofstore',
      data: {
        "pageIndex" : this.data.pageIndex,
        "pageSize" : 10,
        "storecode": objectId
      },
      method: `GET`,
      header: {
        'content-type': 'application/json', // 默认值
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res);
        var list1 = that.data.foodsList;
        for (var i = 0; i < res.data.data.length; i++) {
          var item = res.data.data[i];
          var url = "https://laoguidiaoyu.oss-cn-shanghai.aliyuncs.com/" + item.image;
          item.image = decodeURIComponent(url);
          console.log(url);
          list1.push(item);
        }

        that.setData({
          foodsList: list1,
        })

      }
    })
  },
  

  makePhoneCall() {
    let that = this;
    // console.log(this.phoneNum);
    var phone = that.data.phoneNum;
    console.log(phone);
    wx.makePhoneCall({
      phoneNumber : phone,
      success() {
        console.log('成功拨打电话')
      }
    })
  }
})