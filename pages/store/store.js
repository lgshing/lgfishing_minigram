//获取应用实例
var app = getApp()
import { rootUrl } from '../../utils/params'
Page({
  data: {
    longitude:'',
    lagitude : '',
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    // loadingHidden: false,  // loading
    pageIndex: 1,
    userInfo: '',
    userDate: ''
  },
  onShareAppMessage() {
    return {
      title: '老G钓鱼',
      desc: '老G饵料社区交流平台',
      path: '/pages/index'
    }
  },
  onLoad() {
    let that = this;
    that.fetchSearchList();
    wx.getLocation({
      success(res) {
        console.log(res)
        that.setData({
          longitude : res.longitude,
          latitude: res.latitude
        })
      }
    })
  },

  onPullDownRefresh: function () {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    that.setData({
      pageIndex: 1
    })
    that.fetchSearchList();
  },

  onReachBottom: function () {
    let that = this;
    console.log('加载更多');
    var index = that.data.pageIndex + 1;
    that.setData({
      pageIndex: index
    })
    that.fetchSearchList();
  },

  fetchSearchList: function () {
    let that = this;
    wx.request({
      url: 'https://www.heikengdiaoyu.com:9405/getnearby?',
      data: {
        'latlng': '30.607455,104.087513',
        'pageIndex': that.data.pageIndex,
        'pageSize': 20
      },
      method: `GET`,
      header: {
        'content-type': 'application/json', // 默认值
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res)
        var list = that.data.userDate;

        if (that.data.pageIndex == 1) {
          list = [];
        }
        for (var i = 0; i < res.data.data.length; i++) {
          var item = res.data.data[i];
          if (item.distance>1000){
            item.distance = (item.distance / 1000).toFixed(2) + 'km';
          }else{
            item.distance = item.distance + 'm';
          }
          list.push(item);
        }
        that.setData({
          loadingHidden: true,
          userDate: list
        })
        wx.hideNavigationBarLoading();
      }
    })
  }
})
