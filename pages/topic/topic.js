//获取应用实例
var app = getApp()
import { rootUrl } from '../../utils/params'
Page({
  data: {
    pageIndex: 1,
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    // loadingHidden: false,  // loading
    userInfo: '',
    userData: []
  },
  
  onLoad() {
    var that = this
    this.fetchSearchList();
  },
  
  onPullDownRefresh: function () {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    that.setData({
      pageIndex: 1
    })
    that.fetchSearchList();
  },

  onReachBottom: function () {
    let that = this;
    console.log('加载更多');
    var index = that.data.pageIndex + 1;
    that.setData({
      pageIndex: index
    })
    that.fetchSearchList();
  },

  fetchSearchList: function () {
    let that = this;
    wx.request({
      url: 'https://www.heikengdiaoyu.com:9401/listing/list?',
      data: {
        'navId': 1,
        'pageIndex': that.data.pageIndex,
        'pageSize': 10,
      },
      method: `GET`,
      header: {
        'content-type': 'application/json', // 默认值
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res)
        var list = that.data.userData;

        if (that.data.pageIndex == 1) {
          list = [];
        }
        for (var i = 0; i < res.data.data.length; i++) {
          var item = res.data.data[i];
          var imgList = item.smallImg.split(',');
          
          for(var imageIndex = 0; imageIndex < imgList.length; imageIndex++){
            imgList[imageIndex] = "https://laoguidiaoyu.oss-cn-shanghai.aliyuncs.com/" + imgList[imageIndex];
          }
          var headerUrl = "https://laoguidiaoyu.oss-cn-shanghai.aliyuncs.com/" + item.userPortrait;

          item.userPortrait = decodeURIComponent(headerUrl);
          item.imageList = imgList;
          var dateTime = new Date(item.createTime)
          var month = dateTime.getMonth() + 1;
          var day = dateTime.getDate();
          item.timeStr = month + '-' + day;
          list.push(item);
        }
        console.log(list);
        that.setData({
          loadingHidden: true,
          userData: list
        })
        wx.hideNavigationBarLoading();
      }
    })
  }
})
