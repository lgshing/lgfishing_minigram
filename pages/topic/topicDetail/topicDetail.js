
import { imageUrl } from '../../../utils/params'
var WxParse = require('../../../wxParse/wxParse.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userData: '',
    article: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.Detail(options.objectId);
    var that = this;
    /**
     * 初始化emoji设置
     */
    WxParse.emojisInit('[]', "/wxParse/emojis/", {
      "00": "00.gif",
      "01": "01.gif",
      "02": "02.gif",
      "03": "03.gif",
      "04": "04.gif",
      "05": "05.gif",
      "06": "06.gif",
      "07": "07.gif",
      "08": "08.gif",
      "09": "09.gif",
      "09": "09.gif",
      "10": "10.gif",
      "11": "11.gif",
      "12": "12.gif",
      "13": "13.gif",
      "14": "14.gif",
      "15": "15.gif",
      "16": "16.gif",
      "17": "17.gif",
      "18": "18.gif",
      "19": "19.gif",
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  Detail: function (objectId) {
    let that = this;
    wx.request({
      url: 'https://47.100.13.178:9401/detail?',
      data: {
        "listId" : objectId
      },
      method: `GET`,
      header: {
        'content-type': 'application/json', // 默认值
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res)
        var item = res.data.data;
        console.log(item);
        item.userPortrait = imageUrl + item.userPortrait;
        var dateTime = new Date(item.createTime)
        var year = dateTime.getYear();
        var month = dateTime.getMonth() + 1;
        var day = dateTime.getDate();
        item.createTime = year + '-' + month + '-' + day;
        that.setData({
          userData: item
        })
        WxParse.wxParse('article', 'html', item.html_content, that, 5);
      }
    })
  }
})