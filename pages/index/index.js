//获取应用实例
var app = getApp()
import { rootUrl } from '../../utils/params'
Page({
  data: {
    proLst: [],
    pageIndex: 1,
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    loadingHidden: false,  // loading
    userInfo: '',
    userDate: []
  },
  onPullDownRefresh: function () {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    that.setData({
      pageIndex: 1
    })
    that.fetchSearchList();
  },

  onReachBottom: function () {
    let that = this;
    console.log('加载更多');
    var index = that.data.pageIndex + 1;
    that.setData({
      pageIndex : index
    })
    that.fetchSearchList();
  },

  onShareAppMessage() {
    return {
      title: '老G钓鱼',
      desc: '老G饵料社区交流平台',
      path: '/pages/index'
    }
  },
  onLoad() {
    var that = this
    this.fetchSearchList();
  },

  fetchSearchList: function () {
    let that = this;
    wx.request({
      url: 'https://www.heikengdiaoyu.com:9405/getfoodslist?',
      data: {
        'fish_species_code': 1,
        'foods_species_code': 1,
        'pageIndex': that.data.pageIndex,
        'pageSize': 10,
        'waters_species': 1
      },
      method: `GET`,
      header: {
        'content-type': 'application/json', // 默认值
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res)
        var list = that.data.userDate;

        if(that.data.pageIndex == 1){
          list = [];
        }
        for (var i = 0; i < res.data.data.length; i++) {
          var item = res.data.data[i];
          var url = "https://laoguidiaoyu.oss-cn-shanghai.aliyuncs.com/" + item.image;
          item.image = decodeURIComponent(url);
          console.log(url);
          list.push(item);
        }
        that.setData({
          loadingHidden : true,
          userDate: list
        })
        wx.hideNavigationBarLoading();
      }
    })
  },

 

})
